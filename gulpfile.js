var gulp = require("gulp");
var postcss = require("gulp-postcss");
var postcss_plugins = [
	require("postcss-simple-vars")(),
	require("postcss-svg")({
		paths: ["/tmp/pc/src/svg"],
		debug: true,
		ei: false
	})
];

gulp.task("default", ["build-css"]);

gulp.task("build-css", function() {
	return gulp.src("./src/*.css")
		.pipe(postcss(postcss_plugins))
		.pipe(gulp.dest("./build/"));
});